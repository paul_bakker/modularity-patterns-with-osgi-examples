package org.amdatu.examples.whiteboard.pluginregistry.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.examples.whiteboard.pluginregistry.Plugin;
import org.amdatu.examples.whiteboard.pluginregistry.Registry;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.ServiceReference;

@Component
public class RegistryImpl implements Registry {
	
	private final Map<ServiceReference, Plugin> plugins = new ConcurrentHashMap<>();
	
	@ServiceDependency(removed="pluginRemoved")
	public void pluginAdded(ServiceReference ref, Plugin plugin) {
		plugins.put(ref, plugin);
		System.out.println("Plugin added");
	}
	
	public void pluginRemoved(ServiceReference ref) {
		plugins.remove(ref);
		System.out.println("Plugin removed");
	}

	@Override
	public List<Plugin> listPlugins() {
		return new ArrayList<>(plugins.values());
	}

	@Override
	public Optional<Plugin> getPlugin(String name) {
		return plugins.values().stream()
				.filter(p -> p.getName().equalsIgnoreCase(name))
				.findAny();
	}
	
	
}
