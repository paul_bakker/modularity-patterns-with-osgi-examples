package org.amdatu.examples.whiteboard.pluginregistry;

public interface Plugin {
	String getName();
	void execute();
}
