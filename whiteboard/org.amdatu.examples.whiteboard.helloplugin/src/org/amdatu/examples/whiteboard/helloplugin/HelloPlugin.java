package org.amdatu.examples.whiteboard.helloplugin;

import org.amdatu.examples.whiteboard.pluginregistry.Plugin;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class HelloPlugin implements Plugin{

	@Override
	public String getName() {
		return "hello";
	}

	@Override
	public void execute() {
		System.out.println("Hello, from the Hello Plugin!");
	}

}
