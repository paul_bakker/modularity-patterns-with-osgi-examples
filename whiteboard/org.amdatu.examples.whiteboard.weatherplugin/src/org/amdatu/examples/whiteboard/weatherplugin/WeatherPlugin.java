package org.amdatu.examples.whiteboard.weatherplugin;

import java.io.IOException;
import java.net.URL;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;

import org.amdatu.examples.whiteboard.pluginregistry.Plugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component(properties=@Property(name=Constants.SERVICE_PID, value="org.amdatu.examples.whiteboard.weather"))
/**
 * This plugin reads weather information from Open Weather Map and prints its description to System.out
 *
 */
public class WeatherPlugin implements Plugin, ManagedService {
	
	private volatile String city = "Amsterdam,nl";

	@Override
	public String getName() {
		return "weather";
	}

	@Override
	public void execute() {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			Map<?, ?> readValue = mapper.readValue(new URL("http://api.openweathermap.org/data/2.5/weather?q=" + city), Map.class);
			
			@SuppressWarnings("unchecked")
			List<Map<String,?>> weather = (List<Map<String,?>>) readValue.get("weather");
			weather.stream()
				.map(w -> w.get("description"))
				.forEach(System.out::println);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
		Object newCity = properties.get("city");		
		if(newCity != null) {
			city = (String)newCity;
		}
	}

}
