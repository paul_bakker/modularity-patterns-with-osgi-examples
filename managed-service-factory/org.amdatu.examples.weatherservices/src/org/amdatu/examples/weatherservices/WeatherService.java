package org.amdatu.examples.weatherservices;

public interface WeatherService {

	WeatherInfo getWeatherInfo();
}
