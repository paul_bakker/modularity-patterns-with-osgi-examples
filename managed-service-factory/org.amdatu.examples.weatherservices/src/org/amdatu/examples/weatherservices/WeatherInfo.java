package org.amdatu.examples.weatherservices;

import java.util.List;

public class WeatherInfo {
	private final String city;
	private final double temp;
	private final List<String> descriptions;

	public WeatherInfo(String city, double temp, List<String> descriptions) {
		this.city = city;
		this.temp = temp;
		this.descriptions = descriptions;
	}

	public String getCity() {
		return city;
	}

	public double getTemp() {
		return temp;
	}

	public List<String> getDescriptions() {
		return descriptions;
	}

	@Override
	public String toString() {
		return "WeatherInfo [city=" + city + ", temp=" + temp
				+ ", descriptions=" + descriptions + "]";
	}
}
