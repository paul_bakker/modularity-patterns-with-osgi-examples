package org.amdatu.examples.weatherservices.impl;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.amdatu.examples.weatherservices.WeatherInfo;
import org.amdatu.examples.weatherservices.WeatherService;

import com.fasterxml.jackson.databind.ObjectMapper;

public class WeatherServiceImpl implements WeatherService {

	private final String city;

	public WeatherServiceImpl(String city) {
		this.city = city;
	}

	@SuppressWarnings("unchecked")
	@Override
	public WeatherInfo getWeatherInfo() {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<?, ?> readValue = mapper.readValue(new URL("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric"), Map.class);
			
			List<Map<String,?>> weather = (List<Map<String,?>>) readValue.get("weather");
			
			
			List<String> descriptions = weather.stream()
				.map(w -> (String)w.get("description")).collect(Collectors.toList());
			Map<String,?> main = (Map<String, ?>) readValue.get("main");
			double temp = Double.parseDouble(main.get("temp").toString());
			
			WeatherInfo info = new WeatherInfo(city, temp, descriptions);
			return info;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
