package org.amdatu.examples.weatherservices.impl;

import java.util.Dictionary;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.examples.weatherservices.WeatherService;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;

@Component(properties=@Property(name=Constants.SERVICE_PID, value="org.amdatu.examples.weatherservice"))
public class WeatherFactory implements ManagedServiceFactory {

	@Inject
	private volatile DependencyManager dm;
	private final Map<String, org.apache.felix.dm.Component> components = new ConcurrentHashMap<>();
	
	@Override
	public String getName() {
		return "Weather Service";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		
		String city = (String) properties.get("city");
		WeatherServiceImpl weatherService = new WeatherServiceImpl(city);
		org.apache.felix.dm.Component c = dm.createComponent().setInterface(WeatherService.class.getName(), properties).setImplementation(weatherService);
		
		components.put(pid, c);
		dm.add(c);
	}

	@Override
	public void deleted(String pid) {
		dm.remove(components.remove(pid));
	}
	
	
	
}
