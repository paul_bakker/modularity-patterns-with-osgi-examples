package org.amdatu.examples.weathermonitor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.examples.weatherservices.WeatherService;
import org.amdatu.scheduling.annotations.RepeatForever;
import org.amdatu.scheduling.annotations.RepeatInterval;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.ServiceReference;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Component
@RepeatForever
@RepeatInterval(period = RepeatInterval.SECOND, value = 10)
public class WeatherMonitor implements Job {

	private final Map<ServiceReference, WeatherService> weatherServices = new ConcurrentHashMap<>();

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		
		weatherServices.values().parallelStream()
			.map(WeatherService::getWeatherInfo)
			.forEach(System.out::println);
	}


	@ServiceDependency(removed="removeWeatherService")
	public void addWeatherService(ServiceReference ref, WeatherService ws) {
		weatherServices.put(ref, ws);
	}
	
	public void removeWeatherService(ServiceReference ref) {
		weatherServices.remove(ref);
	}
}
