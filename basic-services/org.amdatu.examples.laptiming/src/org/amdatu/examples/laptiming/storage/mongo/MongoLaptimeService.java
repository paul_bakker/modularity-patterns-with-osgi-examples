package org.amdatu.examples.laptiming.storage.mongo;

import java.util.ArrayList;
import java.util.List;

import org.amdatu.examples.laptiming.storage.api.Laptime;
import org.amdatu.examples.laptiming.storage.api.LaptimingService;
import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

@Component
public class MongoLaptimeService implements LaptimingService{

	@ServiceDependency
	private volatile MongoDBService mongoDbService;
	private volatile MongoCollection laptimes;
	
	@Start
	public void start() {
		Jongo jongo = new Jongo(mongoDbService.getDB());
		laptimes = jongo.getCollection("laptimes");
		
		prepareTestData();
	}

	
	
	@Override
	public void storeResult(Laptime time) {
		laptimes.save(time);
		
	}

	@Override
	public List<Laptime> listForTrack(String track) {
		
		List<Laptime> result = new ArrayList<>();
		
		laptimes.find("{track: #}", track).as(Laptime.class).forEach(result::add);
		
		return result;
	}

	private void prepareTestData() {
		laptimes.remove();
		
		laptimes.save(new Laptime("Zandvoort", "Driver A", 60 * 1000 + 26*1000 + 959));
		laptimes.save(new Laptime("Suzuka", "Driver A", 60 * 1000 + 31*1000 + 540));
		laptimes.save(new Laptime("Nurburgring", "Driver A", 6 * 60 * 1000 + 48*1000));
		
		laptimes.save(new Laptime("Zandvoort", "Driver B", 60 * 1000 + 27*1000));
		laptimes.save(new Laptime("Suzuka", "Driver B", 60 * 1000 + 32));
		laptimes.save(new Laptime("Nurburgring", "Driver B", 6 * 60 * 1000 + 49*1000));
		
		laptimes.save(new Laptime("Zandvoort", "Driver C", 60 * 1000 + 28*1000));
		laptimes.save(new Laptime("Suzuka", "Driver C", 60 * 1000 + 33));
		laptimes.save(new Laptime("Nurburgring", "Driver C", 6 * 60 * 1000 + 450*1000));
	}
}
