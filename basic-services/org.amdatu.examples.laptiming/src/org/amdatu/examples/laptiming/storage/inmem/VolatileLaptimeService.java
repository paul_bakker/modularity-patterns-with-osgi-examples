package org.amdatu.examples.laptiming.storage.inmem;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.amdatu.examples.laptiming.storage.api.Laptime;
import org.amdatu.examples.laptiming.storage.api.LaptimingService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class VolatileLaptimeService implements LaptimingService{

	private final List<Laptime> laptimes = new CopyOnWriteArrayList<>();
	
	@Start
	public void start() {
		prepareTestData();
	}
	
	@Override
	public void storeResult(Laptime time) {
		laptimes.add(time);
	}

	@Override
	public List<Laptime> listForTrack(String track) {
		return laptimes.stream()
				.filter(t -> t.getTrack().equalsIgnoreCase(track))
				.collect(Collectors.toList());
	}

	private void prepareTestData() {
		
		laptimes.add(new Laptime("Zandvoort", "Driver A", 10));
		laptimes.add(new Laptime("Suzuka", "Driver A", 20));
		laptimes.add(new Laptime("Nurburgring", "Driver A", 10));
		
		laptimes.add(new Laptime("Zandvoort", "Driver B", 20));
		laptimes.add(new Laptime("Suzuka", "Driver B", 10));
		laptimes.add(new Laptime("Nurburgring", "Driver B", 20));
		
		laptimes.add(new Laptime("Zandvoort", "Driver C", 30));
		laptimes.add(new Laptime("Suzuka", "Driver C", 30));
		laptimes.add(new Laptime("Nurburgring", "Driver C", 30));
	}
}
