package org.amdatu.examples.laptiming.storage.api;

public class Laptime {
	private String _id;
	private String track;
	private String driver;
	private long time;
	
	public Laptime() {
	}
	
	public Laptime(String track, String driver, long time) {
		this.track = track;
		this.driver = driver;
		this.time = time;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
