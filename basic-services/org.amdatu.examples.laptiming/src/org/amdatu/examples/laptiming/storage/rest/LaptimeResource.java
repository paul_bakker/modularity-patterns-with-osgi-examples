package org.amdatu.examples.laptiming.storage.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.examples.laptiming.storage.api.Laptime;
import org.amdatu.examples.laptiming.storage.api.LaptimingService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("laptimes")
@Component(provides=Object.class)
public class LaptimeResource {
	@ServiceDependency
	private volatile LaptimingService laptimeService;
	
	@Path("{track}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Laptime> list(@PathParam("track") String track) {
		return laptimeService.listForTrack(track);
	}
}
