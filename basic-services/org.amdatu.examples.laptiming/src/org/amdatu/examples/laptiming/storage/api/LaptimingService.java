package org.amdatu.examples.laptiming.storage.api;

import java.util.List;

public interface LaptimingService {
	void storeResult(Laptime time);
	List<Laptime> listForTrack(String track);
}
