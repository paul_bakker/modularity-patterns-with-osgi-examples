package org.amdatu.examples.stats;

import java.util.List;

public interface StatsService {
	List<DriverStats> getDriverStats();
}
