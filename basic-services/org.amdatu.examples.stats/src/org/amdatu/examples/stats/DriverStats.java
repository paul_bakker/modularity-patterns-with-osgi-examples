package org.amdatu.examples.stats;

public class DriverStats implements Comparable<DriverStats>{
	private final String name;
	private final int points;

	public DriverStats(String name, int points) {
		this.name = name;
		this.points = points;
	}

	public String getName() {
		return name;
	}

	public int getPoints() {
		return points;
	}
	
	@Override
	public String toString() {
		return "DriverStats [name=" + name + ", points=" + points
				+ ", getName()=" + getName() + ", getPoints()=" + getPoints()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	@Override
	public int compareTo(DriverStats o) {
		
		if(points > o.points) {
			return 1;
		} else if(points < o.points) {
			return -1;
		} else {
			return 0;
		}
	}
}
