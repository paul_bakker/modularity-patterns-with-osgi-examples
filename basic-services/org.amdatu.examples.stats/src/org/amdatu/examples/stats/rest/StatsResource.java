package org.amdatu.examples.stats.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.examples.stats.DriverStats;
import org.amdatu.examples.stats.StatsService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("stats")
@Component(provides=Object.class)
public class StatsResource {
	
	@ServiceDependency
	private volatile StatsService statsService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<DriverStats> getStats() {
		return statsService.getDriverStats();
	}

}
