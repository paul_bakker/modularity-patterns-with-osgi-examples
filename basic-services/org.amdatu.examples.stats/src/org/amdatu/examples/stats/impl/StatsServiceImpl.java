package org.amdatu.examples.stats.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.amdatu.examples.laptiming.storage.api.Laptime;
import org.amdatu.examples.laptiming.storage.api.LaptimingService;
import org.amdatu.examples.stats.DriverStats;
import org.amdatu.examples.stats.StatsService;
import org.amdatu.examples.tracks.storage.TrackService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class StatsServiceImpl implements StatsService {
	@ServiceDependency
	private volatile TrackService trackService;

	@ServiceDependency
	private volatile LaptimingService laptimingService;

	@Override
	public List<DriverStats> getDriverStats() {
		Map<String, Integer> points = new HashMap<>();

		trackService
				.list()
				.stream()
				.map(t -> laptimingService.listForTrack(t).stream()
						.sorted(new TimeComparator()).map(Laptime::getDriver)
						.findFirst())
				.forEach(
						d -> {

							if (d.isPresent()) {
								Integer pointsForDriver = points.getOrDefault(
										d.get(), 0);
								points.put(d.get(), pointsForDriver + 1);
							}
						});

		return points.entrySet().stream()
				.map(e -> new DriverStats(e.getKey(), e.getValue())).sorted()
				.collect(Collectors.toList());
	}

	class TimeComparator implements Comparator<Laptime> {

		@Override
		public int compare(Laptime o1, Laptime o2) {

			if (o1.getTime() > o2.getTime()) {
				return 1;
			} else if (o1.getTime() < o2.getTime()) {
				return -1;
			} else {
				return 0;
			}
		}

	}
}
