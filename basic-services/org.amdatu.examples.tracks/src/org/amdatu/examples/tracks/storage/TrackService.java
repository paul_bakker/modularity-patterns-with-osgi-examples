package org.amdatu.examples.tracks.storage;

import java.util.List;

public interface TrackService {

	List<String> list();
}
