package org.amdatu.examples.tracks.storage.simple;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.amdatu.examples.tracks.storage.TrackService;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class SimpleTrackService implements TrackService{

	private final List<String> tracks = Collections.unmodifiableList(Arrays.asList("Zandvoort", "Suzuka", "Nurburgring"));
	
	@Override
	public List<String> list() {
		return tracks;
	}

}
