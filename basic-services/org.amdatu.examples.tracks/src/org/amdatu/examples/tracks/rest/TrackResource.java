package org.amdatu.examples.tracks.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.examples.tracks.storage.TrackService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("tracks")
@Component(provides=Object.class)
public class TrackResource {
	
	@ServiceDependency
	private volatile TrackService trackService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> listTracks() {
		return trackService.list();
	}
}
