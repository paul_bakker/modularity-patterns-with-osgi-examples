package org.amdatu.examples.greeter.impl;

import org.amdatu.examples.greeter.Greeter;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class GreeterImpl implements Greeter{

	@Override
	public String sayHello() {
		return "hello";
	}

}
