package org.amdatu.examples.welcome;

import org.amdatu.examples.greeter.Greeter;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class Welcome {
	
	@ServiceDependency
	private volatile Greeter greeter;
	
	@Start
	public void start() {
		System.out.println(greeter.sayHello());
	}
}
