package org.amdatu.examples.extender;

import java.util.Dictionary;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.Servlet;

import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.BundleDependency;
import org.apache.felix.dm.annotation.api.Component;
import org.osgi.framework.Bundle;

/**
 * Note that this is a simplified example with no error checking. Don't use this
 * code :-)
 */
@Component
public class BundleExtender {
	
	private final Map<Bundle, DependencyManager> components = new ConcurrentHashMap<>();

	@BundleDependency(stateMask=Bundle.ACTIVE, filter="(X-example-web=*)", removed="bundleRemoved")
	public void bundleAdded(Bundle b) {
		System.out.println("Bundle " + b.getSymbolicName() + " contains the x-example-web header");
		
		Dictionary<String, String> headers = b.getHeaders();

		String webHeader = headers.get("X-example-web");
		String[] split = webHeader.split("=");
		String path = split[0];
		String dir = split[1];

		DependencyManager dm = new DependencyManager(b.getBundleContext());

		Properties props = new Properties();
		props.put("alias", path);

		org.apache.felix.dm.Component servlet = dm.createComponent()
				.setInterface(Servlet.class.getName(), props)
				.setImplementation(new StaticFileServlet(dir, b));
		
		dm.add(servlet);
		components.put(b, dm);
	}
	

	public void bundleRemoved(Bundle b) {
		DependencyManager dm = components.remove(b);
		if(dm != null) {
			dm.clear();
		}
	}	
}
