package org.amdatu.examples.extender;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;

public class StaticFileServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	private final String dir; 
	private final Bundle bundle;
	
	public StaticFileServlet(String dir, Bundle bundle) {
		this.dir = dir;
		this.bundle = bundle;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = dir + req.getPathInfo();
		System.out.println("Getting resource " + name);
		URL resource = bundle.getResource(name);
		
		if(name.endsWith(".js")) {
			resp.setContentType("application/javascript");
		} else if(name.endsWith(".css")) {
			resp.setContentType("text/css");
		} else if(name.endsWith(".jpg")) {
			resp.setContentType("image/jpeg");
		}
		
		try(InputStream in = resource.openStream()) {
			IOUtils.copy(in, resp.getOutputStream());
		}
		
	}
}
